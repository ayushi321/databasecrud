package com.piet;

public class CRUD_Mongo implements Icrud,Iconnect {
    @Override
    public void connect() {

        System.out.println("Mongo connected successfully!");
    }

    @Override
    public void disconnect() {

    }

    @Override
    public void create() {

    }

    @Override
    public void read() {

    }

    @Override
    public void update() {

    }

    @Override
    public void delete() {

    }
}
