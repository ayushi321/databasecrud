package com.piet;

public class DbUtils {

    private String DbURL , username , password,query;

    private Dbtype dbType;

    public DbUtils(String dbURL, String username, String password, String query, Dbtype dbType) {
        DbURL = dbURL;
        this.username = username;
        this.password = password;
        this.query = query;
        this.dbType = dbType;
    }

    //switch
    // mongo
    // mysql
    // cassandra
    public void factory(){

        switch (dbType){
            case MONGO :
                CRUD_Mongo crud_mongo = new CRUD_Mongo();

                crud_mongo.connect();
                crud_mongo.create();
                break;
            case MYSQL:
                CRUD_RDBMS crud_rdbms = new CRUD_RDBMS();
                break;
            case CASSANDRA:
                CRUD_Cassandra crud_cassandra = new CRUD_Cassandra();
                break;
            default:
                System.out.println("invalid option");
        }


    }

}
